const fs = require('fs');

const handler = (req, res) => {
  fs.readFile(__dirname + '/index.html',
    (err, data) => {
      if( err ){
        res.writeHead(500);
        return res.end('Error loading index.html');
      }
      res.writeHead(200);
      res.end(data);
  });
};  

const app = require('http').createServer(handler);

const io = require('socket.io')(app);


function serve( port=4444 ){
  console.log('serve()');
  this.init = () => {
    console.log('init()');
    app.listen( port );
    io.on('connection', (sock) => {
      console.log('connection');
      sock.emit('news', { hello: 'World!'} );
      sock.on('hi', (data) => {
        console.log('hi');
        console.log(data);
      });
    });
  }; 
  return this ;  
};

module.exports = serve;

