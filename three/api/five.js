console.log(`--- ${process.argv[1]} ---`);

const cmd = process.argv[2];
const params = process.argv.splice(3);

const { spawn } = require('child_process');
const ls = spawn('docker', [ 'run', '-v', '$PWD:/usr/src/app',  'node:alpine', 'npm', 'run', 'three']);

ls.stdout.on('data', (data) => {
  console.log(`stdout: ${data}`);
});

ls.stderr.on('data', (data) => {
  console.error(`stderr: ${data}`);
});

ls.on('close', (code)=>{
  console.log(`Child Process exited with code: ${code}`);
});

console.log(`~~~ ${process.argv[1]} ~~~`);
