console.log(`--- ${process.argv[1]} ---`);

const { spawn } = require('child_process');
const ls = spawn('ls', ['-ltra', '../..']);

ls.stdout.on('data', (data) => {
  console.log(`stdout: ${data}`);
});

ls.stderr.on('data', (data) => {
  console.error(`stderr: ${data}`);
});

ls.on('close', (code)=>{
  console.log(`Child Process exited with code: ${code}`);
});

console.log(`~~~ ${process.argv[1]} ~~~`);
