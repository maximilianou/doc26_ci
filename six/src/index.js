import _ from 'lodash'
import './style.css'
const component = () => {
  const elem = document.createElement('div')
  elem.innerHTML = _.join(['Hello', 'WebPack!'], ' ')
  elem.classList.add('hello')
  return elem
}
document.body.appendChild(component())
