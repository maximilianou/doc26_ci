const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')

module.exports = {
  mode: 'development',
  entry: {
    app: './src/index.js',
    print: './src/print.js'
  },
  devtool: 'inline-source-map',
  devServer: {
    contentBase: './dist',
    host: '0.0.0.0',
    port: 9010,
    before: function (app, server) {
      //      app.get('/some/path', function (req, res) {
      //        res.json({ custom: 'response' })
      //      })
    },
    after: function (app, server) {

    }
    // http2: true,
    // https: {
    //  key: fs.readFileSync('/path/to/server.key'),
    //  cert: fs.readFileSync('/path/to/server.crt'),
    //  ca: fs.readFileSync('/path/to/ca.pem'),
    // }
  },
  plugins: [
    new CleanWebpackPlugin(),
    new HtmlWebpackPlugin({
      title: 'Development'
    })
  ],
  output: {
    filename: '[name].bundle.js',
    path: path.resolve(__dirname, 'dist')
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          'style-loader',
          'css-loader'
        ]
      }
    ]
  }
}
