const express = require('express');
const bodyParser = require('body-parser');
const server = function( port = 4444 ){
  this.app = express();
  this.app.use(bodyParser.json());
  this.app.use(bodyParser.urlencoded( { extended: true } ));
  //const http = require('http').createServer(this.app);
  const http = require('http').Server(this.app);
  this.io = require('socket.io')(http);
    this.io.on('connection', ( socketServer ) => {
      socketServer.on('npmStop', () => {
        console.log('[..] Been killed');
        process.exit(0);
        console.log('[Ok] Been killed');
      });
    });

  this.app.get('/', (req, res) => {
    console.log(`ApiMock: ${req.body} `);
    res.send('Hello, here we are !!')
  });

  this.app.post('/msg', (req, res) => {
    console.log(`ApiMock: ${req.body} `);
    console.log(`ApiMock:JSON:  ${JSON.stringify(req.body)} `);
    res.status( 204 ).send(JSON.stringify( { id: 123, message: 'Hello POsting'} ));
  });

  this.start = async () => {
    await this.app.listen(port, () => console.log(`Example ApiMock listening on port ${port}!`));
  };
  this.stop = () => {
    
  };
  
  return this;
}


module.exports = server;

