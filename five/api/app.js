const app = require('express')();
const server = require('http').Server(app);
const io = require('socket.io')(server);

server.listen(port=4444);
app.get('/', (req, res) => {
  res.sendFile(__dirname + '/index.html'); 
});
io.on('connection', (socketAccept) => {
  socketAccept.emit('news', { hello: 'This message from the Server.'})
  socketAccept.on('other event', (data) => {
    console.log(`Server:: ${JSON.stringify(data)}`);
  });
});

