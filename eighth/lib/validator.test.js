const validator = require('./validator');


const usernameLength = {
  field: 'username',
  test: o => o.username.length >= 6,
  message: 'Username must be at least 6 characters',
};

const passwordMatch = {
  field: 'password',
  test: o => o.password === o.confirmPassword,
  message: '',
};

describe('validator', () => {
  it('should return true with an object with no criteria', () => {
    const obj = { username: 'max13' };
    expect( validator(obj, null).valid === true );
  });
  it('should pass an object that meets a criteria', () => {
    const obj = { username: 'max123'};
    const criteria = [usernameLength];
    expect( validator(obj, criteria).valid === true ); 
  });
  it('should pass an object that meets a criteria', () => {
    const obj = { username: 'max12'};
    const criteria = [usernameLength];
    expect( validator(obj, criteria).valid === false ); 
  });
  it('should return true if all criteria pass', () => {
    const obj = { 
                  username: 'max123', 
                  password: '123456', 
                  confirmPassword: '123456' };
    const criteria = [
      usernameLength,
      passwordMatch,
    ];
    expect( validator(obj, criteria).valid === true  );
  });
  it('should return true if all criteria pass', () => {
    const obj = { 
                  username: 'max123', 
                  password: '123456', 
                  confirmPassword: '123' };
    const criteria = [
      usernameLength,
      passwordMatch,
    ];
    expect( validator(obj, criteria).valid === false  );
  });
  it('should contain a failed error message', () => {
    const obj = { username: 'max12' };
    const criteria = [ usernameLength ];
    expect( validator(obj, criteria)).toEqual({
      valid: false,
      errors: {
        username: ['Username must be at least 6 characters']
      }
    });
  });
});
