const validator = (obj, criteria) => {
  const cleanCriteria = criteria || [];
  const errors = cleanCriteria.reduce( (messages, criterion) => {
    const { field, test, message } = criterion;
    if(!test(obj)){
      messages[field] ? messages[field].push(message) : (messages[field] = [message]);
    }
    return messages;
  }, {});
  return { 
    valid: Object.keys(errors).length === 0, 
    errors 
  };
}

module.exports = validator;

